import utfpr.ct.dainf.if62c.pratica.Matriz;
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica51 {

    public static void main(String[] args) throws MatrizInvalidaException {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        Matriz secsoma = new Matriz(3, 2);
        double [][]c = secsoma.getMatriz();

        try{
            m[0][0] = 2;
            m[0][1] = 4;
            m[1][0] = 6;
            m[1][1] = 8;
            m[2][0] = 10;
            m[2][1] = 12;


            c[0][0] = 1;
            c[0][1] = 3;
            c[1][0] = 5;
            c[1][1] = 7;
            c[2][0] = 9;
            c[2][1] = 11;


            Matriz transp = orig.getTransposta();
            Matriz s = orig.soma(secsoma);


            Matriz mult = orig.prod(transp);


            System.out.println("Matriz original: " + orig);
            System.out.println("Matriz transposta: " + transp);

            System.out.println("Matriz soma: " + s);

            System.out.println("Matriz produto: " + mult);
        }
        catch(MatrizInvalidaException | RuntimeException a){
            System.out.println(a.getLocalizedMessage());
        }
    }
}
 