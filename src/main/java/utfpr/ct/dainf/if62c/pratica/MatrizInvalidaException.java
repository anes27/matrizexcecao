/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Gamer
 */
public class MatrizInvalidaException extends Exception {
    
    private final int numlinhas;
    private final int numcolunas;
    
    public MatrizInvalidaException(int linhas, int colunas){
        super(String.format("Matriz de %dx%d não pode ser criada", linhas,colunas));
        this.numlinhas = linhas;
        this.numcolunas = colunas;
    }
    
    public int getNumLinhas()
    {
        return numlinhas;
    }
    
    
    public int getNumColunas()
    {
        return numcolunas;
    }
}

